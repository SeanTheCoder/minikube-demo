FROM openjdk:11.0.1

COPY ./minikube-demo /opt/minikube-demo
WORKDIR /opt/minikube-demo
RUN chmod +x ./gradlew && ./gradlew clean build

EXPOSE 8080/tcp

CMD export H2_URL="$(cat /etc/configmaps/minikube-demo/db-h2/url)" && \
    export H2_USERNAME="$(cat /etc/secrets/minikube-demo/db-h2/username)" && \
    export H2_PASSWORD="$(cat /etc/secrets/minikube-demo/db-h2/password)" && \
    ./gradlew bootRun
