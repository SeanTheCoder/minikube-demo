# Minikube Demo

This project demonstrates a single-machine 3-tier architecture with Minikube.

* Data Tier ([H2](http://www.h2database.com))
* Web Tier ([Spring Boot](https://spring.io/projects/spring-boot) and [Thymeleaf](https://www.thymeleaf.org))
* Front-end Tier ([Kubernetes Services](https://kubernetes.io/docs/concepts/services-networking/service))

## TL;DR

To create & start the 3-tier architecture:

```powershell
kubectl create -f ./k8s/k8s-all.yml
```

Wait until all pods are ready (status: `ContainerCreating` -> `Running`) when running the following:

(Note that due to the image pulling process, this takes approximately **8 minutes** for the first run during testing.)

```powershell
kubectl get pods --watch
```

To open the web service in your browser:

```powershell
minikube service web-sb
```

To stop & delete the 3-tier architecture:

```powershell
kubectl delete -f ./k8s/k8s-all.yml
```

## Testing Environment

This project is developed and tested in an environment as below:

|Item       |Version        |
|:----------|:-------------:|
|OS         |Windows 10 Pro |
|Minikube   |v0.30.0        |
|Kubernetes |v1.10.0        |

## Architecture Diagram

![Architecture Diagram](images/architecture-diagram.png)

## Notes

File `k8s/k8s-all.yml` is generated with commands below:

```powershell
Get-Content -Encoding UTF8 ./k8s/k8s-cm.yml, ./k8s/k8s-secret.yml, ./k8s/k8s-pv.yml, ./k8s/k8s-pvc.yml, ./k8s/k8s-svc.yml, ./k8s/k8s-deploy.yml | Out-File -Encoding utf8 -FilePath ./k8s/k8s-all.yml
```

The architecture can be alternatively started with commands below:

```powershell
Get-Content -Encoding UTF8 ./k8s/k8s-cm.yml, ./k8s/k8s-secret.yml, ./k8s/k8s-pv.yml, ./k8s/k8s-pvc.yml, ./k8s/k8s-svc.yml, ./k8s/k8s-deploy.yml | kubectl create -f -
```

To access H2 database directly:

```powershell
minikube service db-h2
```

* **H2 URL**: jdbc:h2:tcp://localhost:1521/file:/opt/h2-data/minikubedemodb
* **H2 Username**: demo
* **H2 Password**: minikube

## Room for Improvement

### Database Initialisation

When Spring Boot web server (web-sb) has a replica number greater than one, all pods will try to initialise H2 database and cause database initialisation to be attempted for multiple times. The current workaround is set `continue-on-error: true` to avoid pods failing with errors.

A more proper solution would be using [Liquibase](https://www.liquibase.org) to manage database initialisation.

### Unit Testing

Considering this is a demo project, no unit testing has been created currently.
