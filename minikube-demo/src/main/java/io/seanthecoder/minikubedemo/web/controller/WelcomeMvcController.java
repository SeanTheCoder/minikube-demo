package io.seanthecoder.minikubedemo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.seanthecoder.minikubedemo.service.HostDetailAccessor;

@Controller
@RequestMapping(value = "/web")
public class WelcomeMvcController {

	@Autowired
	private HostDetailAccessor hostDetailAccessor;

	@GetMapping(value = "/welcome")
	public String getAllPlanets(Model model) {

		String hostAddress = hostDetailAccessor.getHostAddress();
		model.addAttribute("hostAddress", hostAddress);

		return "welcome";
	}
}
