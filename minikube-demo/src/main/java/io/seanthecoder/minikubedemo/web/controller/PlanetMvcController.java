package io.seanthecoder.minikubedemo.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.seanthecoder.minikubedemo.jpa.entity.Planet;
import io.seanthecoder.minikubedemo.service.HostDetailAccessor;
import io.seanthecoder.minikubedemo.service.PlanetAccessor;

@Controller
@RequestMapping(value = "/web/planet")
public class PlanetMvcController {

	@Autowired
	private HostDetailAccessor hostDetailAccessor;

	@Autowired
	private PlanetAccessor planetAccessor;

	@GetMapping(value = "all")
	public String getAllPlanets(Model model) {

		String hostAddress = hostDetailAccessor.getHostAddress();
		List<Planet> planets = planetAccessor.getAllPlanets();

		model.addAttribute("hostAddress", hostAddress);
		model.addAttribute("planets", planets);

		return "planets-all";
	}
}
