package io.seanthecoder.minikubedemo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.seanthecoder.minikubedemo.jpa.entity.Planet;
import io.seanthecoder.minikubedemo.service.PlanetAccessor;

@SpringBootApplication
public class MinikubeDemoApplication {

	private static final Logger logger = LoggerFactory.getLogger(MinikubeDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MinikubeDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner testTransactionAccessor(PlanetAccessor planetAccessor) {

		return args -> {

			logger.info("--------------------");
			logger.info("***** planetAccessor.getAllPlanets() *****");

			List<Planet> planets = planetAccessor.getAllPlanets();
			planets.forEach(item -> logger.info(item.toString()));
		};
	}
}
