package io.seanthecoder.minikubedemo.jpa.repo;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import io.seanthecoder.minikubedemo.jpa.entity.Planet;

@Transactional(readOnly = true)
public interface PlanetRepository extends CrudRepository<Planet, Long>, JpaSpecificationExecutor<Planet> {
}
