package io.seanthecoder.minikubedemo.jpa.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "planet")
@Table(name = "planet")
@ToString
public class Planet {

	@Id
	@Getter
	@Setter
	@Column(name = "planet_id")
	private Long planetId;

	@Getter
	@Setter
	@Column(name = "planet_order")
	private Integer planetOrder;

	@Getter
	@Setter
	@Column(name = "planet_name")
	private String planetName;

	@Getter
	@Setter
	@Column(name = "planet_diameter_km")
	private Integer planetDiameterKm;

	@Getter
	@Setter
	@Column(name = "planet_size_to_earth")
	private BigDecimal planetSizeToEarth;
}
