package io.seanthecoder.minikubedemo.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import io.seanthecoder.minikubedemo.jpa.entity.Planet;
import io.seanthecoder.minikubedemo.jpa.repo.PlanetRepository;

@Service
@Primary
public class PlanetAccessorImpl implements PlanetAccessor {

	@Autowired
	private PlanetRepository planetRepository;

	@Override
	public List<Planet> getAllPlanets() {

		Specification<Planet> spec = null;
		Sort sortByPlanetOrder = new Sort(Sort.Direction.ASC, "planetOrder");
		Iterable<Planet> planets = planetRepository.findAll(spec, sortByPlanetOrder);
		Boolean isParallel = Boolean.FALSE;

		return StreamSupport.stream(planets.spliterator(), isParallel).collect(Collectors.toList());
	}
}
