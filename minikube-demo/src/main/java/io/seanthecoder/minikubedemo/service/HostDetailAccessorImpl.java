package io.seanthecoder.minikubedemo.service;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class HostDetailAccessorImpl implements HostDetailAccessor {

	@Override
	public String getHostAddress() {

		String result = null;

		try {

			InetAddress inetAddress = InetAddress.getLocalHost();
			result = inetAddress.getHostAddress();

		} catch (UnknownHostException e) {

			result = "UNKNOWN_HOST";
		}

		return result;
	}
}
