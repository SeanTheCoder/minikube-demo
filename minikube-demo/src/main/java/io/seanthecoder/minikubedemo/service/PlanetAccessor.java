package io.seanthecoder.minikubedemo.service;

import java.util.List;

import io.seanthecoder.minikubedemo.jpa.entity.Planet;

public interface PlanetAccessor {

	public List<Planet> getAllPlanets();
}
