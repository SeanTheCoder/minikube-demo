package io.seanthecoder.minikubedemo.service;

public interface HostDetailAccessor {

	public String getHostAddress();
}
