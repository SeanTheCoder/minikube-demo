-- Populate table(s).
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 8, 'Neptune',	49528, 		3.9);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 7, 'Uranus',	51118, 		4.0);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 6, 'Saturn',	120536, 	9.4);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 5, 'Jupiter',	142984, 	11.2);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 4, 'Mars',		6792, 		0.5);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 3, 'Earth',	12756, 		1.0);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 2, 'Venus',	12104, 		0.9);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 1, 'Mercury',	4879, 		0.4);
INSERT INTO planet(planet_id, planet_order, planet_name, planet_diameter_km, planet_size_to_earth) VALUES (NULL, 0, 'Sun', 		1391000,	109.0);

-- ********** Bottom Placeholder **********
