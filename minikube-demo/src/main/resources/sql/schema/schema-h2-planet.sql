-- Create table(s).
CREATE TABLE IF NOT EXISTS planet(
  planet_id				IDENTITY		NOT NULL,
  planet_order  		INT       		NOT NULL,
  planet_name			VARCHAR(32)		NOT NULL,
  planet_diameter_km	INT				NOT NULL,
  planet_size_to_earth	DECIMAL			NOT NULL,
  
  CONSTRAINT	pk_planet_id        	PRIMARY KEY(planet_id ASC),
  CONSTRAINT    uq_planet_order   		UNIQUE(planet_order),
  CONSTRAINT    uq_planet_name      	UNIQUE(planet_name)
);
